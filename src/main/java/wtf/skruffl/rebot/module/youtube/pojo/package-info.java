
@XmlSchema(namespace = "http://www.w3.org/2005/Atom", elementFormDefault = XmlNsForm.QUALIFIED, xmlns = {
        @XmlNs(prefix = "yt", namespaceURI = "http://www.youtube.com/xml/schemas/2015"),
        @XmlNs(prefix = "", namespaceURI = "http://www.w3.org/2005/Atom") })
package wtf.skruffl.rebot.module.youtube.pojo;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
