package wtf.skruffl.rebot.module.youtube.pojo;

import lombok.Data;

@Data
public class Author {
    private String name;

    private String uri;
}