package wtf.skruffl.rebot.module.youtube.pojo;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.annotation.XmlElement;

import com.google.gson.Gson;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@ToString
@Slf4j
public class Entry {
    @XmlElement(namespace = "http://www.w3.org/2005/Atom")
    private Author author;

    @XmlElement(namespace = "http://www.w3.org/2005/Atom")
    private Link link;

    @XmlElement(namespace = "http://www.w3.org/2005/Atom")
    private String id;

    @XmlElement(namespace = "http://www.youtube.com/xml/schemas/2015")
    private String videoId;
    @XmlElement(namespace = "http://www.youtube.com/xml/schemas/2015")
    private String channelId;

    @XmlElement(namespace = "http://www.w3.org/2005/Atom")
    private String published;

    @XmlElement(namespace = "http://www.w3.org/2005/Atom")
    private String title;

    @XmlElement(namespace = "http://www.w3.org/2005/Atom")
    private String updated;

    public String fetchIcon(String apiToken) {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            URIBuilder builder = new URIBuilder("https://www.googleapis.com/youtube/v3/channels");
            builder.addParameter("part", "snippet");
            builder.addParameter("id", getChannelId());
            builder.addParameter("fields", "items/snippet/thumbnails");
            builder.addParameter("key", apiToken);
            HttpGet get = new HttpGet(builder.build());
            CloseableHttpResponse response = client.execute(get);
            String readFromInputStream = readFromInputStream(response.getEntity().getContent());
            YoutubeQueryResult result = new Gson().fromJson(readFromInputStream, YoutubeQueryResult.class);
            return result.getItems()[0].getSnippet().getThumbnails().getHigh().getUrl();
        } catch (Exception e) {
            log.error("Couldn't fetch youtube icon", e);
        }
        return null;
    }

    public String getThumbnailUrl() {
        return String.format("https://img.youtube.com/vi/%s/maxresdefault.jpg", getVideoId());
    }

    private String readFromInputStream(InputStream content) throws IOException {
        try (InputStream stream = content) {
            StringBuilder builder = new StringBuilder();
            int i = 0;
            while ((i = content.read()) != -1) {
                builder.append((char) i);
            }
            return builder.toString();
        }
    }

    @Data
    class YoutubeQueryResult {
        private Result[] items;

        @Data
        class Result {
            private Snippet snippet;

            @Data
            class Snippet {
                private Thumbnail thumbnails;

                @Data
                class Thumbnail {
                    private Holder def;
                    private Holder medium;
                    private Holder high;

                    @Data
                    class Holder {
                        private String url;
                        private int width;
                        private int height;
                    }
                }
            }
        }
    }
}