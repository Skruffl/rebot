package wtf.skruffl.rebot.module.youtube.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Link
 */
public class Link extends XmlAdapter<Link, String> {

    @XmlAttribute
    public String rel = "self";

    @XmlAttribute
    public String href;

    @Override
    public String unmarshal(Link v) throws Exception {
        return v.href;
    }

    @Override
    public Link marshal(String v) throws Exception {
        Link link = new Link();
        link.href = v;
        return link;
    }

}