package wtf.skruffl.rebot.module.youtube;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.WebUtils;
import wtf.skruffl.rebot.core.db.RepoHolder;
import wtf.skruffl.rebot.core.db.SettingType;
import wtf.skruffl.rebot.core.db.SubscriptionType;
import wtf.skruffl.rebot.core.db.dao.Setting;
import wtf.skruffl.rebot.core.db.dao.Subscription;
import wtf.skruffl.rebot.module.youtube.pojo.Entry;
import wtf.skruffl.rebot.module.youtube.pojo.Feed;

@RestController
@Slf4j
@EnableScheduling
@RequestMapping("/api/v1/youtube")
public class YoutubeSubscriptionController {

    @Getter
    private Bot bot;
    private RepoHolder repositories;
    private int port;

    private Set<String> uploadedVideos = new HashSet<>();

    public YoutubeSubscriptionController(@Autowired Bot bot) {
        this.bot = bot;
        this.repositories = bot.getRepos();
        this.port = bot.getPort();
        log.debug("Created new YoutubeSubscriptionController");
    }

    @PostMapping
    public String receiveSomething(@RequestBody Feed pushNotif) {
        log.debug("Received youtube endpoint xml: \n{}", pushNotif);
        newStream(pushNotif);
        return "{\"message\":\"Success!\"}";
    }

    @GetMapping
    public String challenge(
            @RequestParam(name = "hub.challenge", defaultValue = "No challenge present") String challenge) {
        log.debug("Received challenge: {}", challenge);
        return challenge;
    }

    @Scheduled(fixedRate = 1800000l, initialDelay = 1000l) // 30 minutes
    private void subscribeAll() {
        log.debug("Subscribing to all youtube channels");
        for (Subscription adsfjok : repositories.getSubscriptionRepo().findAllByType(SubscriptionType.YouTube)) {
            subscribe(adsfjok);
        }
    }

    public void addYoutuber(Subscription sub) {
        if (sub.getType() != SubscriptionType.YouTube) {
            throw new IllegalArgumentException("Subscription is not a youtuber");
        }

        repositories.getSubscriptionRepo().save(sub);
        subscribe(sub);
    }

    private void subscribe(Subscription sub) {
        log.debug("Subscribing to youtuber {}", sub.getLink());
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            URIBuilder builder = new URIBuilder("https://pubsubhubbub.appspot.com/subscribe");
            builder.setParameter("hub.mode", "subscribe");
            builder.setParameter("hub.verify", "async");
            builder.setParameter("hub.callback", WebUtils.getInstance().getCallbackUrl(port) + "api/v1/youtube/");
            builder.setParameter("hub.topic", toFeed(sub.getLink()));
            builder.setParameter("hub.lease_seconds", Long.toString(TimeUnit.MINUTES.toSeconds(30)));
            HttpPost post = new HttpPost(builder.build().toString());
            HttpResponse reply = client.execute(post);
            String content = WebUtils.getInstance().readFromInputStream(reply.getEntity().getContent());
            log.debug("Received content from youtube\n{}", content);
        } catch (IOException | URISyntaxException e) {
            log.error("Couldn't send posts", e);
        }
    }

    private String toFeed(String link) {
        if (link.startsWith("https://www.youtube.com/xml/feeds")) {
            return link;
        }
        return link.replace("https://www.youtube.com/channel/",
                "https://www.youtube.com/xml/feeds/videos.xml?channel_id=");
    }

    public void newStream(Feed pushNotif) {
        if (pushNotif.getEntry() == null) {
            return;
        }
        String link = pushNotif.getEntry().getLink().href;
        if (uploadedVideos.contains(link)) {
            return;
        } else {
            uploadedVideos.add(link);
        }
        for (Subscription sub : repositories.getSubscriptionRepo().findAllByType(SubscriptionType.YouTube)) {
            // we'll get an empty entry if the video was deleted
            if (pushNotif.getEntry() != null && sub.getLink().equals(pushNotif.getEntry().getAuthor().getUri())) {
                sendMessage(sub, pushNotif);
            }
        }
    }

    private void sendMessage(Subscription sub, Feed pushNotif) {

        Guild guild = bot.getJda().getGuildById(sub.getGuildId());
        Setting notificationChannel = repositories.fetchSetting(SettingType.YOUTUBE_NOTIFICATION_CHANNEL,
                guild.getIdLong());
        TextChannel channel = null;
        if (notificationChannel == null) {
            channel = guild.getDefaultChannel();
        } else {
            channel = notificationChannel.asChannel(guild);
        }

        Role pingRole = repositories.fetchSetting(SettingType.NOTIFICATION_ROLE_ID, guild.getIdLong()).asRole(guild);
        String name = pushNotif.getEntry().getAuthor().getName();
        // update the name in our database in case it changed
        if (!name.equals(sub.getName())) {
            sub.setName(name);
            repositories.getSubscriptionRepo().save(sub);
        }

        Setting useEmbed = bot.getRepos().fetchSetting(SettingType.YOUTUBE_USE_EMBED, sub.getGuildId());
        if (useEmbed == null || !"true".equalsIgnoreCase(useEmbed.getValue())) {
            sendClassicMessage(sub, pushNotif, channel, pingRole);
        } else {
            sendEmbedMessage(sub, pushNotif, channel, pingRole);
        }
    }

    private void sendEmbedMessage(Subscription sub, Feed pushNotif, TextChannel channel, Role pingRole) {
        Entry entry = pushNotif.getEntry();

        String name = entry.getAuthor().getName();
        EmbedBuilder builder = new EmbedBuilder();
        builder.setAuthor(name, pushNotif.getEntry().getAuthor().getUri(),
                entry.fetchIcon(bot.getConfigDao().getYtApiToken()));
        builder.setTitle(name + " uploaded a video!");
        builder.setDescription(pushNotif.getEntry().getLink().href);
        builder.addField("Video Title", pushNotif.getEntry().getTitle(), false);

        LocalDateTime parse = LocalDateTime.parse(pushNotif.getEntry().getPublished(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"));
        builder.setTimestamp(parse);
        builder.setImage(entry.getThumbnailUrl());

        MessageBuilder msgBuilder = new MessageBuilder();
        msgBuilder.setEmbed(builder.build());
        if (pingRole != null && sub.isShouldPing()) {
            String content = String.format("Hey %s\n**%s** just posted a new video!", pingRole.getAsMention(), name);
            msgBuilder.setContent(content);
        }

        channel.sendMessage(msgBuilder.build()).queue();
    }

    private void sendClassicMessage(Subscription sub, Feed pushNotif, TextChannel channel, Role pingRole) {
        Entry entry = pushNotif.getEntry();
        String content = String.format("**%s** just posted a video!\nGo check it out!\n%s", entry.getAuthor().getName(),
                entry.getLink().href);
        if (pingRole != null && sub.isShouldPing()) {
            content = "Hey " + pingRole.getAsMention() + "\n\n" + content;
        }
        channel.sendMessage(content).queue();
    }

}
