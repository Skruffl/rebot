package wtf.skruffl.rebot.module.youtube.msg;

import java.util.LinkedHashMap;
import java.util.List;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.db.SubscriptionType;
import wtf.skruffl.rebot.core.db.dao.Subscription;
import wtf.skruffl.rebot.core.db.repo.SubscriptionRepository;
import wtf.skruffl.rebot.core.message.PrivilegedSubcommandNameMatchingHandler;
import wtf.skruffl.rebot.module.youtube.YoutubeSubscriptionController;

public class AddYoutubeSubscriptionMessageHandler extends PrivilegedSubcommandNameMatchingHandler {

    private YoutubeSubscriptionController ctrl;

    public AddYoutubeSubscriptionMessageHandler(YoutubeSubscriptionController ctrl) {
        super("add", "yt");
        this.ctrl = ctrl;
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        if (getArgs().length < 1) {
            return false;
        }

        Bot bot = ctrl.getBot();
        String channelUrl = getArgs()[0];
        boolean shouldPing = getArgs().length > 1 ? "true".equalsIgnoreCase(getArgs()[1]) : false;
        if (!channelUrl.contains("youtube.com/channel/")) {
            return false;
        }

        // remove all parameters attached to the url
        if (channelUrl.contains("?")) {
            channelUrl = channelUrl.substring(0, channelUrl.lastIndexOf("?"));
        }

        String channelId = channelUrl.substring(channelUrl.lastIndexOf("/channel/")).replace("/channel/", "");

        Subscription sub = null;
        SubscriptionRepository subRepo = bot.getRepos().getSubscriptionRepo();
        List<Subscription> subscriptions = subRepo.findAllByTypeAndGuildId(SubscriptionType.YouTube,
                event.getGuild().getIdLong());
        // check if a sub for the channel exists and remove if there is
        for (Subscription subscription : subscriptions) {
            if (subscription.getLink().contains(channelId)) {
                event.getChannel().sendMessage("Removed channel " + channelUrl).queue();
                subRepo.delete(subscription);
                return true;
            }
        }

        // else create a new one
        if (sub == null) {
            sub = new Subscription();
        }

        sub.setGuildId(event.getGuild().getIdLong());
        sub.setLink(channelUrl);
        sub.setType(SubscriptionType.YouTube);
        sub.setShouldPing(shouldPing);

        subRepo.save(sub);
        ctrl.addYoutuber(sub);
        event.getChannel().sendMessage("Added channel " + channelUrl).queue();
        return true;
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put("channelUrl",
                "The link to the youtube channel (has to be a youtube.com/channel/ link, youtube.com/user/ links won't work!");
        args.put("shouldPing",
                "Optional: if the notification role should be pinged for this YouTube channel (can be true or false)");
        event.getChannel().sendMessage(buildSyntax(ctrl.getBot().getConfigDao().getPrefix(), "add yt", args)).queue();
    }

}