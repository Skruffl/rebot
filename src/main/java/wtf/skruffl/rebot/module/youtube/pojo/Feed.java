
package wtf.skruffl.rebot.module.youtube.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement
@Data
public class Feed {
    private Entry entry;

    private String xmlns;
    private Link[] link;

    private String title;

    private String updated;
}