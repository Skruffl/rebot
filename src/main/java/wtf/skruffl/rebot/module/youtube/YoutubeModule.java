package wtf.skruffl.rebot.module.youtube;

import java.util.Arrays;
import java.util.List;

import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.youtube.msg.AddYoutubeSubscriptionMessageHandler;

public class YoutubeModule extends BotModule {

    private static final String NAME = "youtube";
    private Bot bot;

    public YoutubeModule(CommandRegistry registry, Bot bot) {
        super(NAME, registry);
        this.bot = bot;
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new AddYoutubeSubscriptionMessageHandler(
                bot.getContext().getBean(YoutubeSubscriptionController.class)));
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

}