package wtf.skruffl.rebot.module.standard.msg;

import java.util.HashMap;
import java.util.Map;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.db.SettingType;
import wtf.skruffl.rebot.core.db.dao.Setting;
import wtf.skruffl.rebot.core.db.repo.SettingRepository;
import wtf.skruffl.rebot.core.message.PrivilegedMessageHandler;

public class GuildSettingsMessageHandler extends PrivilegedMessageHandler {

    private static final String NAME = "set";

    public GuildSettingsMessageHandler() {
        super(NAME);
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        String[] args = getArgs();

        switch (args.length) {
            case 1:
                fetchValueAndShowToUser(findTypeForString(args[0]), event);
                return true;
            case 2:
                // continue function
                break;
            default:
                return false;
        }

        String type = args[0];
        String value = args[1];

        SettingType settingType = findTypeForString(type);

        if (settingType == null) {
            return false;
        }

        // remove all control characters
        value = value.replace("<", "").replace(">", "").replace("@", "").replace("#", "").replace("&", "")
                .replaceFirst("\\:.*\\:", "");

        Setting setting = new Setting();

        // Update existing setting if something already exists
        SettingRepository settingRepo = getBot().getRepos().getSettingRepo();
        Setting existingSetting = getBot().getRepos().fetchSetting(settingType, event.getGuild().getIdLong());
        if (existingSetting != null) {
            setting = existingSetting;
        }

        setting.setGuildId(event.getGuild().getIdLong());
        setting.setType(settingType);
        setting.setValue(value);

        settingRepo.save(setting);

        event.getChannel()
                .sendMessage(String.format("Set setting %s to %s", setting.getType().name(), setting.getValue()))
                .queue();

        return true;
    }

    private void fetchValueAndShowToUser(SettingType type, MessageReceivedEvent event) {
        Setting existingSetting = getBot().getRepos().fetchSetting(type, event.getGuild().getIdLong());
        String messageText = "The setting %s is set to %s" + "\n As TextChannel: <#%s> \n" + "As User: <@%s> \n"
                + "As Group: <@&%s>";
        event.getChannel()
                .sendMessage(String.format(messageText, existingSetting.getType().name(), existingSetting.getValue(),
                        existingSetting.getValue(), existingSetting.getValue(), existingSetting.getValue()))
                .queue();
    }

    private SettingType findTypeForString(String type) {
        SettingType settingType = null;
        for (SettingType iterator : SettingType.values()) {
            if (iterator.name().equalsIgnoreCase(type)) {
                settingType = iterator;
                break;
            }
        }
        return settingType;
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        StringBuilder sb = new StringBuilder();
        for (SettingType type : SettingType.values()) {
            sb.append(type.name()).append("  ");
        }

        Map<String, String> args = new HashMap<>();
        args.put("Type", "The type of setting you want to edit, has to be one of \n" + sb.toString());
        args.put("Value", "The value you want to set the setting to");
        event.getChannel().sendMessage(buildSyntax(getBot().getConfigDao().getPrefix(), NAME, args)).queue();
    }

}