package wtf.skruffl.rebot.module.standard;

import java.util.Arrays;
import java.util.List;

import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.standard.msg.GuildSettingsMessageHandler;
import wtf.skruffl.rebot.module.standard.msg.ShowSubscriptionsMessageHandler;

public class StandardModule extends BotModule {

    private static final String NAME = "STANDARD";

    public StandardModule(CommandRegistry registry) {
        super(NAME, registry);
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new GuildSettingsMessageHandler(),
                new ShowSubscriptionsMessageHandler());
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

}