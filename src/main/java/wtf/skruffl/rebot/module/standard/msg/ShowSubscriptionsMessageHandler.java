package wtf.skruffl.rebot.module.standard.msg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.db.SubscriptionType;
import wtf.skruffl.rebot.core.db.dao.Subscription;
import wtf.skruffl.rebot.core.db.repo.SubscriptionRepository;
import wtf.skruffl.rebot.core.message.PrivilegedMessageHandler;

public class ShowSubscriptionsMessageHandler extends PrivilegedMessageHandler {

    private static final String NAME = "listsubs";

    public ShowSubscriptionsMessageHandler() {
        super(NAME);
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        String[] args = getArgs();
        List<Subscription> subs = null;
        SubscriptionType type = getType(args.length > 0 ? args[0] : null);
        SubscriptionRepository subRepo = getBot().getRepos().getSubscriptionRepo();
        if (args.length > 0 && type != null) {
            subs = subRepo.findAllByTypeAndGuildId(type, event.getGuild().getIdLong());
        } else {
            subs = subRepo.findAllByGuildId(event.getGuild().getIdLong());
        }

        Map<SubscriptionType, List<Subscription>> map = new HashMap<>();
        for (Subscription sub : subs) {
            SubscriptionType subType = sub.getType();
            if (map.containsKey(subType)) {
                List<Subscription> list = map.get(subType);
                list.add(sub);
                map.put(subType, list);
            } else {
                List<Subscription> list = new ArrayList<>();
                list.add(sub);
                map.put(subType, list);
            }
        }

        EmbedBuilder builder = new EmbedBuilder();

        for (Map.Entry<SubscriptionType, List<Subscription>> entry : map.entrySet()) {
            builder.addField("Platform", entry.getKey().name(), false);
            for (Subscription sub : entry.getValue()) {
                String name = sub.getName();
                if (name == null) {
                    name = "Subscription";
                }
                String value = sub.getLink() + "\n";
                value += sub.isShouldPing() ? "mentions role" : "does not mention role";
                builder.addField(name, value, true);
            }
        }

        if (map.isEmpty()) {
            builder.setDescription("No subscriptions available for this guild");
        }

        MessageBuilder msgBuilder = new MessageBuilder(builder.build());
        if (type != null) {
            msgBuilder.setContent(
                    String.format("Showing %s subscriptions for guild %s", type.name(), event.getGuild().getName()));
        }

        event.getChannel().sendMessage(msgBuilder.build()).queue();

        return true;
    }

    private SubscriptionType getType(String string) {
        for (SubscriptionType type : SubscriptionType.values()) {
            if (type.name().equalsIgnoreCase(string)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        // not sent
    }

}