package wtf.skruffl.rebot.module.privatechannels;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.db.dao.PrivateRoom;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.privatechannels.msg.CreatePrivateRoomSettingMessageHandler;

public class PrivateChannelsModule extends BotModule {

    private static final String NAME = "PrivateChannels";
    private Bot bot;

    public PrivateChannelsModule(CommandRegistry registry, Bot bot) {
        super(NAME, registry);
        this.bot = bot;
    }

    public void onGuildVoiceJoin(@Nonnull GuildVoiceJoinEvent event) {
        createChannel(event.getGuild(), event.getMember(), event.getChannelJoined());
    }

    private void createChannel(Guild guild, Member member, VoiceChannel joinedChannel) {
        Optional<PrivateRoom> optionalRoom = bot.getRepos().getPrivateRoomRepo().findOneByGuildId(guild.getIdLong());
        if (!optionalRoom.isPresent()) {
            // no creation room specified
            return;
        }
        PrivateRoom room = optionalRoom.get();
        if (joinedChannel.getIdLong() != room.getRoomId()) {
            // room joined is not the creation room
            return;
        }

        Category category = guild.getCategoryById(room.getCategoryId());
        String channelName = getChannelName(member);
        VoiceChannel voiceChannel = category.createVoiceChannel(channelName).complete();
        voiceChannel.createPermissionOverride(member)
                .setAllow(Permission.VOICE_MOVE_OTHERS, Permission.VOICE_CONNECT, Permission.VOICE_SPEAK).queue();
        voiceChannel.putPermissionOverride(guild.getPublicRole()).deny(Permission.VOICE_CONNECT).queue();

        VoiceChannel moveChannel = category.createVoiceChannel("Waiting for move [" + member.getEffectiveName() + "]")
                .complete();
        moveChannel.createPermissionOverride(member).setAllow(Permission.VOICE_MOVE_OTHERS).queue();
        moveChannel.putPermissionOverride(guild.getPublicRole()).grant(Permission.VOICE_CONNECT)
                .deny(Permission.VOICE_SPEAK).queue();

        guild.moveVoiceMember(member, voiceChannel).queue();

        // allow bot to delete channels
        voiceChannel.createPermissionOverride(guild.getSelfMember()).setAllow(Permission.ALL_CHANNEL_PERMISSIONS)
                .queue();
        moveChannel.createPermissionOverride(guild.getSelfMember()).setAllow(Permission.ALL_CHANNEL_PERMISSIONS)
                .queue();
    }

    private String getChannelName(Member member) {
        return member.getEffectiveName() + " [Private Room]";
    }

    public void onGuildVoiceLeave(@Nonnull GuildVoiceLeaveEvent event) {
        removeChannel(event.getMember(), event.getChannelLeft());
    }

    private void removeChannel(Member member, VoiceChannel channel) {
        String name = getChannelName(member);
        if (channel.getName().equalsIgnoreCase(name)) {
            List<VoiceChannel> channels = member.getGuild()
                    .getVoiceChannelsByName("Waiting for move [" + member.getEffectiveName() + "]", true);
            for (VoiceChannel vc : channels) {
                vc.delete().queue();
            }
            channel.delete().queue();
        }
    }

    public void onGuildVoiceMove(@Nonnull GuildVoiceMoveEvent event) {
        removeChannel(event.getMember(), event.getChannelLeft());
        createChannel(event.getGuild(), event.getMember(), event.getChannelJoined());
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new CreatePrivateRoomSettingMessageHandler());
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

}