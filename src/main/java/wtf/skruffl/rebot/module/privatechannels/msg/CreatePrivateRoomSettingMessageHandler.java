package wtf.skruffl.rebot.module.privatechannels.msg;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.db.dao.PrivateRoom;
import wtf.skruffl.rebot.core.message.PrivilegedMessageHandler;

public class CreatePrivateRoomSettingMessageHandler extends PrivilegedMessageHandler {

    private static final String NAME = "privroom";

    public CreatePrivateRoomSettingMessageHandler() {
        super(NAME);
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        String[] args = getArgs();
        if (args.length < 2) {
            return false;
        }

        VoiceChannel vc = null;
        Category category = null;

        if (isNumber(args[0])) {
            vc = event.getGuild().getVoiceChannelById(args[0]);
        } else {
            vc = event.getGuild().getVoiceChannelsByName(args[0], true).get(0);
        }

        if (isNumber(args[1])) {
            category = event.getGuild().getCategoryById(args[1]);
        } else {
            category = event.getGuild().getCategoriesByName(args[1], true).get(0);
        }

        PrivateRoom room = new PrivateRoom();
        Optional<PrivateRoom> optionalRoom = getBot().getRepos().getPrivateRoomRepo()
                .findOneByGuildId(event.getGuild().getIdLong());
        if (optionalRoom.isPresent()) {
            room = optionalRoom.get();
        }

        room.setGuildId(event.getGuild().getIdLong());
        room.setCategoryId(category.getIdLong());
        room.setRoomId(vc.getIdLong());
        getBot().getRepos().getPrivateRoomRepo().save(room);
        return true;
    }

    private boolean isNumber(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("channel", "The name or the id of the voice channel used to create private rooms");
        map.put("category", "The name or the id of the category to create private channels in");
        event.getChannel().sendMessage(buildSyntax(getBot().getConfigDao().getPrefix(), NAME, map)).queue();
    }
}