package wtf.skruffl.rebot.module.inpanix;

import java.util.Arrays;
import java.util.List;

import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.inpanix.msg.PichuEmbedCreatorMessageHandler;
import wtf.skruffl.rebot.module.inpanix.msg.VacancyEmbedCreatorMessageHandler;

public class VacancyModule extends BotModule {

    private static final String NAME = "Vacancy";

    public VacancyModule(CommandRegistry registry) {
        super(NAME, registry);
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new VacancyEmbedCreatorMessageHandler(),
                new PichuEmbedCreatorMessageHandler());
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

}