package wtf.skruffl.rebot.module.inpanix.msg;

import java.awt.Color;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.message.NameMatchingMessageHandler;

public class PichuEmbedCreatorMessageHandler extends NameMatchingMessageHandler {

        private static final String NAME = "pichu_embed";

        public PichuEmbedCreatorMessageHandler() {
                super(NAME);
        }

        @Override
        public boolean handle(MessageReceivedEvent event) {
                switch (getArgs()[0]) {
                        case "welcome":
                                welcome(event);
                                break;
                        case "rules":
                                rules(event);
                                break;
                        case "roster":
                                roster(event);
                                break;
                        case "boost":
                                boost(event);
                                break;
                        case "info":
                                info(event);
                                break;
                        case "submit":
                                submit(event);
                                break;
                        case "server_boost":
                                serverBoost(event);
                                break;
                        default:
                                return false;
                }
                return true;
        }

        private void serverBoost(MessageReceivedEvent event) {
                EmbedBuilder builder = new EmbedBuilder();
                builder.setTitle("Server Boosting");
                builder.addField(EmbedBuilder.ZERO_WIDTH_SPACE,
                                "Boost the server to unlock different perks for yourself and the server! The more boosts we get, the more rewards the whole sever can enjoy!\n"
                                                + "\n"
                                                + "Listed below is a full list of perks you can get for boosting as well as what perks the whole server can get.\n"
                                                + "\n" + "What you get:\n" + "\n"
                                                + "- You will receive @Server Booster role!\n"
                                                + "- A dedicated text and voice channel for all server boosters only!\n"
                                                + "- Priority support from our staff!\n" + "- And more to come!\n"
                                                + "\n" + "What the server gets:",
                                false);
                builder.addField("Tier 1 - (2 Server Boosts):",
                                "• 50 new server emoji slots (A total of 100)!\n" + "• 128 KBPS in voice channels!\n"
                                                + "• Animated Server Icon!\n" + "• Custom server invite background!\n"
                                                + "• Stream to your friends in high quality.\n",
                                false);
                builder.addField("Tier 2 - (15 Server Boosts):", "Boosted servers get everything previous as well as:\n"
                                + "\n" + "• 50 new emoji slots (A total of 150)!\n" + "• 256 KBPS audio quality!\n"
                                + "• A server banner!\n" + "• 50 MB upload limit for all members!\n"
                                + "• 1080P 60FPS go live streams!\n", false);
                builder.addField("Tier 3 - (50 Server Boosts):", "Boosted servers get everything previous as well as:\n"
                                + "\n" + "• 100 new emoji slots (A total of 250)!\n" + "• 384 KBPS audio quality!\n"
                                + "• A vanity URL for the server (Custom URL)!\n"
                                + "• 100 MB upload limit for all members!", false);
                builder.addField("PLEASE NOTE:",
                                "You can cancel your server boost here at any given time, however, this will result in you loosing any perks you gained. Only current boosters will keep their perks.\n"
                                                + "\n" + "Any support shown is highly appreciated and helps us a lot!",
                                false);

                // builder.setDescription(readInputStreamToString(getClass().getResourceAsStream("/boost.txt")));
                builder.setFooter("Thanks for supporting us!",
                                "https://cdn.discordapp.com/attachments/764193176507777054/764497358960197652/Pichu_AVI.png");
                builder.setThumbnail(
                                "https://cdn.discordapp.com/attachments/764193176507777054/764497358960197652/Pichu_AVI.png");
                event.getChannel().sendMessage(builder.build()).queue();
        }

        private void submit(MessageReceivedEvent event) {
                EmbedBuilder builder = new EmbedBuilder();
                builder.setColor(new Color(255, 87, 175));
                builder.setDescription(
                                "To obtain the <@&736043274350886922> role, please post a screenshot in this channel with proof that you have used code 'VacancyEsports' to purchase an item and react with  to this message!");
                event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
        }

        private void info(MessageReceivedEvent event) {
                EmbedBuilder builder = new EmbedBuilder();
                builder.setColor(new Color(255, 87, 175));

                builder.setTitle("**Welcome to the #VacancyFAM Community Area!**");
                builder.setDescription("<#735661445697503293> - Explains what this category is about.\n"
                                + "<#735319634059722773> - Use code 'VacancyEsports' in the Fortnite item shop and provide us with a screenshort with proof to obtain the <@&736043274350886922> role.");

                event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
        }

        private void boost(MessageReceivedEvent event) {
                EmbedBuilder builder = new EmbedBuilder();
                builder.setColor(new Color(255, 87, 175));
                builder.setTitle("**Server Nitro Boosting**");
                builder.addField("**Vacancy Community Boost Perks**",
                                "- You will receive the <@&736992340241154069> role.", false);
                event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
        }

        private void roster(MessageReceivedEvent event) {
                EmbedBuilder builder = new EmbedBuilder();
                builder.setTitle("**Vacancy Roster**");
                builder.setColor(new Color(255, 87, 175));
                builder.addField("**Management**",
                                "🇩🇪 <@681968899075735558> - [Twitter](https://twitter.com/SvNyyyy), [Instagram](https://www.instagram.com/xzsven/), [YouTube](https://www.youtube.com/channel/UCQPxhN_QalO_r5-T4j7cJyg)\n"
                                                + "🇩🇪 <@203866790156042241> [Twitter](https://twitter.com/RaffaelS1337), [Instagram](https://www.instagram.com/raffael.sheikh/)\n🇩🇪 <@146630621425303552> [Twitter](https://twitter.com/Juls23btw)",
                                false);
                builder.addField("**Staff**",
                                "🇬🇷 <@629223269798772766> - [Twitter](https://twitter.com/PenguinVFX), [YouTube](https://www.youtube.com/channel/UC7nQd2MWD-bp9Km4Tz4KQJg), [Instagram](https://www.instagram.com/pxnguinvfx/)"
                                                + "\n🇪🇸<@613697580148326400> - [Twitter](https://twitter.com/PichuFN), [YouTube](https://www.youtube.com/channel/UCgIeMOpRjKWPOxUptYAP-Rg), [Twitch](https://www.twitch.tv/pichufn), [Instagram](https://www.instagram.com/coachpichu/)",
                                false);
                builder.addField("**Fortnite Roster**", EmbedBuilder.ZERO_WIDTH_SPACE, false);
                event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
        }

        private void rules(MessageReceivedEvent event) {
                EmbedBuilder builder = new EmbedBuilder();
                builder.setColor(new Color(255, 87, 175));
                builder.setTitle("**Community Rules**");

                builder.addField(" **Respect** ", "Be respectful.\n"
                                + "Hate-speech, racism, sexual harassment, personal attacks, threats, impersonation, abuse or trolling to the point that people feel uncomfortable is not allowed.",
                                false);

                builder.addField(" **Discussions** ", "Do not start or bring any unnecessary drama.\n"
                                + "Topics such as religion and politics are deemed too inflammatory/controversial and provocative.",
                                false);

                builder.addField(" **Misconduct** ",
                                "Do not send messages at a rapid speed, flood the chat or spam mentions.\n"
                                                + "You will neither publicly nor privately expose any personal information of any member in this server.",
                                false);

                builder.addField(" **Advertisement** ",
                                "Do not advertise UNLESS we specifically state in a channel that it is allowed.\n"
                                                + "This includes, but is not limited to, Discord servers, Twitch, Twitter, PayPal or YouTube.",
                                false);

                builder.addField(" **Terms of Service (ToS)** ", "Do not break the ToS. \n"
                                + "This includes, but is not limited to, the Discord ToS or the Epic Games ToS.\n"
                                + "For Fortnite this includes, but is not limited to, prohibited acts such as buying or selling cosmetics or accounts.\n"
                                + "For Discord this includes, but is not limited to, unsolicited marketing - direct messaging users to advertise.\n"
                                + "\n" + "-> [Epic Games/Fortnite ToS](https://www.epicgames.com/site/en-US/tos)\n"
                                + "-> [Discord Community Guidelines](https://discordapp.com/guidelines)\n"
                                + "-> [Discord Terms of Service](https://discordapp.com/terms)\n"
                                + "-> [Discord New Safety Page](https://discord.com/new/safety)\n", false);

                builder.addField(EmbedBuilder.ZERO_WIDTH_SPACE,
                                "We reserve the right to modify, terminate or otherwise amend our rules at any time without notification in advance. \n"
                                                + "We reserve the right to remove or disable access to any user for any or no reason. \n"
                                                + "We may take these actions without prior notification to you. \n"
                                                + "These rules are established to ensure a pleasant stay and any disagreement may be resolved through DMs.",
                                false);

                event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();

        }

        private void welcome(MessageReceivedEvent event) {
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setColor(new Color(255, 87, 175));

                embedBuilder.setTitle("**Welcome to Pichu Server**");

                // welcome
                embedBuilder.addField("**Server Channels**",
                                "<#764486879776473111> - Server-related information and introduction.", false);

                String serverInfo = "<#813047823712256060> - Main announcements of the server, important updates.\n" + // announcements
                                "<#764486902203023400> - Rules from our Discord.\n" // rules
                                + "<#764486954514907156> - Server boosts can be found here.\n\n" // server-boosts
                ;
                embedBuilder.addField("**Server Information**", serverInfo, false);

                String socialMedia = "<#764498524733440022> - Social Media notifications\n" // social-feed
                                + "<#764486968498716682> - YouTube and Twitch notifications\n\n" // social-feed
                ;
                embedBuilder.addField("**Social Media**", socialMedia, false);

                // // info
                // String community = "<#735661445697503293> - #VacancyFAM Community
                // explained.\n"
                // + "<#735319634059722773> - Submit proof for your VacancyFAM role here.\n";
                // // submit
                // embedBuilder.addField(
                // "** #VacancyFAM Community**
                // ",
                // community, false);

                String global = "<#813132800030801951> - General chat.\n" // general
                                + "<#785260210087657502> - Feel free to ask me any question!\n" // ask-pichu-questions
                                + "<#764487278952448020> - Looking for 1 vs. 1s.\n" // 1v1
                                + "<#764487302709641267> - Looking for trios.\n" // trio
                                + "<#764487335080886322> - Post your own clips here.\n" // clips
                ;

                embedBuilder.addField(" **Chat Section** ", global, false);

                // Roles
                embedBuilder.addField(" **Roles** ",
                                "React with ✅ to obtain the <@&764495664796794911> role, this role will be used to ping in <#764486968498716682>.",
                                false);

                event.getChannel().editMessageById(764499838338990091l, new MessageBuilder(embedBuilder).build())
                                .queue();
                // event.getChannel().sendMessage(new
                // MessageBuilder(embedBuilder).build()).queue();
        }

        @Override
        public void sendSyntax(MessageReceivedEvent event) {
                // nothin, too lazy
        }

}