package wtf.skruffl.rebot.module.inpanix.msg;

import java.awt.Color;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.message.NameMatchingMessageHandler;

public class VacancyEmbedCreatorMessageHandler extends NameMatchingMessageHandler {

    private static final String NAME = "embed";

    public VacancyEmbedCreatorMessageHandler() {
        super(NAME);
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        switch (getArgs()[0]) {
            case "welcome":
                welcome(event);
                break;
            case "rules":
                rules(event);
                break;
            case "roster":
                roster(event);
                break;
            case "boost":
                boost(event);
                break;
            case "info":
                info(event);
                break;
            case "submit":
                submit(event);
                break;
            default:
                return false;
        }
        return true;
    }

    private void submit(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(new Color(255, 87, 175));
        builder.setDescription(
                "To obtain the <@&736043274350886922> role, please post a screenshot in this channel with proof that you have used code 'VacancyEsports' to purchase an item and react with <:vacancy:736216442881441854> to this message!");
        event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
    }

    private void info(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(new Color(255, 87, 175));

        builder.setTitle("**Welcome to the #VacancyFAM Community Area!**");
        builder.setDescription("<#735661445697503293> - Explains what this category is about.\n"
                + "<#735319634059722773> - Use code 'VacancyEsports' in the Fortnite item shop and provide us with a screenshort with proof to obtain the <@&736043274350886922> role.");

        event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
    }

    private void boost(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(new Color(255, 87, 175));
        builder.setTitle("**Server Nitro Boosting**");
        builder.addField("**Vacancy Community Boost Perks**", "- You will receive the <@&736992340241154069> role.",
                false);
        event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
    }

    private void roster(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setTitle("**Vacancy Roster**");
        builder.setColor(new Color(255, 87, 175));
        builder.addField("**Management**",
                "🇩🇪 <@681968899075735558> - [Twitter](https://twitter.com/SvNyyyy), [Instagram](https://www.instagram.com/xzsven/), [YouTube](https://www.youtube.com/channel/UCQPxhN_QalO_r5-T4j7cJyg)\n"
                        + "🇩🇪 <@203866790156042241> [Twitter](https://twitter.com/RaffaelS1337), [Instagram](https://www.instagram.com/raffael.sheikh/)\n🇩🇪 <@146630621425303552> [Twitter](https://twitter.com/Juls23btw)",
                false);
        builder.addField("**Staff**",
                "🇬🇷 <@629223269798772766> - [Twitter](https://twitter.com/PenguinVFX), [YouTube](https://www.youtube.com/channel/UC7nQd2MWD-bp9Km4Tz4KQJg), [Instagram](https://www.instagram.com/pxnguinvfx/)"
                        + "\n🇪🇸<@613697580148326400> - [Twitter](https://twitter.com/PichuFN), [YouTube](https://www.youtube.com/channel/UCgIeMOpRjKWPOxUptYAP-Rg), [Twitch](https://www.twitch.tv/pichufn), [Instagram](https://www.instagram.com/coachpichu/)",
                false);
        builder.addField("**Fortnite Roster**", EmbedBuilder.ZERO_WIDTH_SPACE, false);
        event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();
    }

    private void rules(MessageReceivedEvent event) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setColor(new Color(255, 87, 175));
        builder.setTitle("**Vacancy Esports Community Rules**");

        builder.addField("<:vacancy:736216442881441854> **Respect** <:vacancy:736216442881441854>", "Be respectful.\n"
                + "Hate-speech, racism, sexual harassment, personal attacks, threats, impersonation, abuse or trolling to the point that people feel uncomfortable is not allowed.",
                false);

        builder.addField("<:vacancy:736216442881441854> **Discussions** <:vacancy:736216442881441854>",
                "Do not start or bring any unnecessary drama.\n"
                        + "Topics such as religion and politics are deemed too inflammatory/controversial and provocative.",
                false);

        builder.addField("<:vacancy:736216442881441854> **Misconduct** <:vacancy:736216442881441854>",
                "Do not send messages at a rapid speed, flood the chat or spam mentions.\n"
                        + "You will neither publicly nor privately expose any personal information of any member in this server.",
                false);

        builder.addField("<:vacancy:736216442881441854> **Advertisement** <:vacancy:736216442881441854>",
                "Do not advertise UNLESS we specifically state in a channel that it is allowed.\n"
                        + "This includes, but is not limited to, Discord servers, Twitch, Twitter, PayPal or YouTube.",
                false);

        builder.addField("<:vacancy:736216442881441854> **Terms of Service (ToS)** <:vacancy:736216442881441854>",
                "Do not break the ToS. \n"
                        + "This includes, but is not limited to, the Discord ToS or the Epic Games ToS.\n"
                        + "For Fortnite this includes, but is not limited to, prohibited acts such as buying or selling cosmetics or accounts.\n"
                        + "For Discord this includes, but is not limited to, unsolicited marketing - direct messaging users to advertise.\n"
                        + "\n" + "-> [Epic Games/Fortnite ToS](https://www.epicgames.com/site/en-US/tos)\n"
                        + "-> [Discord Community Guidelines](https://discordapp.com/guidelines)\n"
                        + "-> [Discord Terms of Service](https://discordapp.com/terms)\n"
                        + "-> [Discord New Safety Page](https://discord.com/new/safety)\n",
                false);

        builder.addField(EmbedBuilder.ZERO_WIDTH_SPACE,
                "We reserve the right to modify, terminate or otherwise amend our rules at any time without notification in advance. \n"
                        + "We reserve the right to remove or disable access to any user for any or no reason. \n"
                        + "We may take these actions without prior notification to you. \n"
                        + "These rules are established to ensure a pleasant stay and any disagreement may be resolved through DMs.",
                false);

        event.getChannel().sendMessage(new MessageBuilder(builder).build()).queue();

    }

    private void welcome(MessageReceivedEvent event) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(new Color(255, 87, 175));

        String desc = "Vacancy Esports is a professional Esports organization based in Germany.\n"
                + "It was founded by passionate and experienced gamers.\n"
                + "Our goal is to support, encourage and improve talented players.\n\n";
        embedBuilder.setTitle("**Welcome to Vacancy Esports**");
        embedBuilder.setDescription(desc);

        // welcome
        embedBuilder.addField("<:vacancy:736216442881441854> **Server Channels** <:vacancy:736216442881441854>",
                "<#735313631197003880> - Server-related information and introduction.", false);

        String serverInfo = "<#735320536036540466> - Main announcements of the server, important updates.\n" + // announcements
                "<#735317436215066677> - Rules from our Discord.\n" // rules
                + "<#735317524509622373> - Our roster from our organization.\n" // vacancy-roster
                + "<#735317616943431680> - Server boosts, perks of boosting can be found here.\n\n" // server-boosts
        ;
        embedBuilder.addField("<:vacancy:736216442881441854> **Server Information** <:vacancy:736216442881441854>",
                serverInfo, false);

        String socialMedia = "<#735320867315384400> - Official Vacancy content and news.\n" // news
                + "<#735320897623425074> - YouTube and Twitch notifications from our players.\n" // content-feed
                + "<#735320920230592542> - Twitter and Instagram notifications from our players.\n\n" // social-feed
        ;
        embedBuilder.addField("<:vacancy:736216442881441854> **Vacancy Social Media** <:vacancy:736216442881441854>",
                socialMedia, false);

        // // info
        // String community = "<#735661445697503293> - #VacancyFAM Community
        // explained.\n"
        // + "<#735319634059722773> - Submit proof for your VacancyFAM role here.\n";
        // // submit
        // embedBuilder.addField(
        // "<:vacancy:736216442881441854>** #VacancyFAM Community**
        // <:vacancy:736216442881441854>",
        // community, false);

        String global = "<#735321827500359730> - General chat.\n" // general
                + "<#735613551917531199> - Search for player chat.\n" // lfg
                // + "<#735319296862846977> - VacancyFAM only chat.\n" // vacancy chat
                + "<#735321858873753640> - Post your best Vacancy memes here.\n" // memes
                + "<#735321898979819560> - Post your own clips here.\n\n" // clips
        ;

        embedBuilder.addField("<:vacancy:736216442881441854> **Vacancy Global Section** <:vacancy:736216442881441854>",
                global, false);

        String socials =
                // "<:vacancy:736216442881441854> https://vacancy-esports.gg/\n" // website+
                "<:twitter:736220926089232444> https://twitter.com/VacancyGG\n" // twitter
                        + "<:youtube:736221423546400818> https://www.youtube.com/channel/UCqVhGF4tqAFf95BX-5KOK4w\n" // yt
                        + "<:insta:736221720004001873> https://www.instagram.com/VacancyGG/\n" // insta
                        + "<:discord:736221719286513695> https://discord.gg/Vacancy\n\n" // discord
        ;
        embedBuilder.addField("<:vacancy:736216442881441854>**Social Media**<:vacancy:736216442881441854>", socials,
                false);

        // Roles
        embedBuilder.addField("<:vacancy:736216442881441854> **Roles** <:vacancy:736216442881441854>",
                "React with <:vacancy:736216442881441854> to obtain the <@&736043426226765824> role, this role will be used to ping in <#735320897623425074> & <#735320920230592542>.",
                false);

        event.getChannel().sendMessage(new MessageBuilder(embedBuilder).build()).queue();
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        // nothin, too lazy
    }

}