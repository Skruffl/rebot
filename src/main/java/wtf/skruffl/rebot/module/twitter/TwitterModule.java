package wtf.skruffl.rebot.module.twitter;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import twitter4j.FilterQuery;
import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.db.SettingType;
import wtf.skruffl.rebot.core.db.SubscriptionType;
import wtf.skruffl.rebot.core.db.dao.Setting;
import wtf.skruffl.rebot.core.db.dao.Subscription;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.twitter.msg.AddTwitterSubscriptionMessageHandler;

@Slf4j
@Component
public class TwitterModule extends BotModule {

    private static final String NAME = "Twitter";

    private Twitter twitter;
    private TwitterStream stream;

    private Bot bot;

    private List<Long> twitterIds;

    public TwitterModule(@Autowired CommandRegistry registry, @Autowired Bot bot) {
        super(NAME, registry);
        this.bot = bot;
        ConfigurationBuilder confBuilder = new ConfigurationBuilder();
        String twitterKey = bot.getConfigDao().getTwitterKey();
        String twitterSec = bot.getConfigDao().getTwitterSec();
        String twitterToken = bot.getConfigDao().getTwitterToken();
        String twitterSecret = bot.getConfigDao().getTwitterSecret();

        if (twitterKey == null || twitterSec == null || twitterToken == null || twitterSecret == null) {
            log.warn("Twitter Keys aren't filled, not creating twitter module");
            return;
        }

        confBuilder.setOAuthConsumerKey(twitterKey).setOAuthConsumerSecret(twitterSec).setOAuthAccessToken(twitterToken)
                .setOAuthAccessTokenSecret(twitterSecret);
        twitter = new TwitterFactory(confBuilder.build()).getInstance();

        subscribeToAll();
    }

    public void subscribeToAll() {
        if (stream != null) {
            stream.shutdown();
        }

        log.debug("Subscribing to twitter accounts");
        FilterQuery filter = new FilterQuery();
        twitterIds = new ArrayList<>();
        List<Subscription> subs = bot.getRepos().getSubscriptionRepo().findAllByType(SubscriptionType.Twitter);
        if (subs.isEmpty()) {
            // twitter does not like empty filtes, would throw an exception
            return;
        }
        for (Subscription sub : subs) {
            log.debug("Subscribing to twitter account {}", sub.getLink());
            long userId = getId(sub);
            if (!twitterIds.contains(userId)) {
                twitterIds.add(userId);
            }
        }

        // why is there no nice way to use varargs with lists?
        // this sucks
        long[] longs = new long[twitterIds.size()];
        for (int i = 0; i < longs.length; i++) {
            longs[i] = twitterIds.get(i);
        }

        filter.follow(longs);
        stream = new TwitterStreamFactory().getInstance(twitter.getAuthorization());
        stream.addListener(new TwitterStatusListener() {
            @Override
            public void onException(Exception ex) {
                log.error("Twitter Exception", ex);
            }

            @Override
            public void onStatus(Status status) {
                if (!status.isRetweet() && twitterIds.contains(status.getUser().getId())
                        && status.getInReplyToStatusId() < 0) {
                    log.info("Twitter user {} posted a new tweet: {}", status.getUser().getName(), status.getText());
                    newTweet(status);
                }
            }
        });
        stream.filter(filter);
    }

    protected void newTweet(Status status) {
        for (Subscription sub : bot.getRepos().getSubscriptionRepo().findAllByType(SubscriptionType.Twitter)) {
            if (sub.getLink().toLowerCase().contains(status.getUser().getScreenName().toLowerCase())) {
                sendMessage(sub, status);
            }
        }
    }

    private void sendMessage(Subscription sub, Status tweet) {

        Guild guild = bot.getJda().getGuildById(sub.getGuildId());
        TextChannel tweetChannel;
        Setting setting = bot.getRepos().fetchSetting(SettingType.TWITTER_NOTIFICATION_CHANNEL, guild.getIdLong());
        if (setting == null) {
            tweetChannel = guild.getDefaultChannel();
        } else {
            tweetChannel = guild.getTextChannelById(setting.getValue());
        }

        Role role = null;
        Setting roleSetting = bot.getRepos().fetchSetting(SettingType.NOTIFICATION_ROLE_ID, sub.getGuildId());
        if (roleSetting != null) {
            role = roleSetting.asRole(guild);
        }

        // Decide which embed type to use
        Setting embedSetting = bot.getRepos().fetchSetting(SettingType.TWITTER_USE_EXPERIMENTAL_EMBED,
                sub.getGuildId());
        if (embedSetting == null || !"true".equalsIgnoreCase(embedSetting.getValue())) {
            sendClassicMessage(sub, tweet, tweetChannel, role);
            return;
        }

        String msg = tweet.getUser().getName() + " has tweeted!";
        String permalink = buildLink(tweet);
        EmbedBuilder embedBuilder = new EmbedBuilder().setThumbnail(tweet.getUser().get400x400ProfileImageURL())
                .setTimestamp(tweet.getCreatedAt().toInstant()).addField(msg, tweet.getText(), false)
                .addField("Link", permalink, false);
        MessageAction fileMessage = tweetChannel.sendMessage(new MessageBuilder(EmbedBuilder.ZERO_WIDTH_SPACE).build());
        boolean sendFileMessage = false;
        if (tweet.getMediaEntities().length == 1) {
            embedBuilder.setImage(tweet.getMediaEntities()[0].getMediaURLHttps());
        } else {
            for (MediaEntity entity : tweet.getMediaEntities()) {
                if (entity.getType().contentEquals("video")) {
                    embedBuilder.setFooter("This tweet contains a video. Open in browser to view.");
                }
                try {
                    sendFileMessage = true;
                    fileMessage.addFile(new URI(entity.getMediaURLHttps()).toURL().openStream(),
                            getMediaEntityName(entity));
                } catch (IOException | URISyntaxException e) {
                    log.error("Error while reading twitter files", e);
                }
            }
        }

        MessageBuilder builder = new MessageBuilder();
        builder.setEmbed(embedBuilder.build());

        if (role != null && sub.isShouldPing()) {
            builder.setContent(String.format("Hey %s\n\n**%s** posted a new tweet!", role.getAsMention(),
                    tweet.getUser().getName()));
        }

        tweetChannel.sendMessage(builder.build()).complete();
        if (sendFileMessage) {
            fileMessage.queue();
        }
    }

    private void sendClassicMessage(Subscription sub, Status tweet, TextChannel tweetChannel, Role role) {
        MessageBuilder builder = new MessageBuilder();
        String content = String.format("**%s** posted a new Tweet!\n%s", tweet.getUser().getName(), buildLink(tweet));
        builder.setContent(content);
        if (role != null && sub.isShouldPing()) {
            builder.setContent(String.format("Hey %s\n\n%s", role.getAsMention(), content));
        }
        tweetChannel.sendMessage(builder.build()).queue();
    }

    private String getMediaEntityName(MediaEntity entity) {
        switch (entity.getType()) {
            case "photo":
                return entity.getId() + ".jpg";
            case "animated_gif":
                return entity.getId() + ".gif";
            default:
                throw new IllegalArgumentException();
        }
    }

    private String buildLink(Status tweet) {
        StringBuilder builder = new StringBuilder();
        builder.append("https://twitter.com/").append(tweet.getUser().getScreenName()).append("/status/")
                .append(tweet.getId());
        return builder.toString();
    }

    private long getId(Subscription sub) {
        try {
            String name = sub.getLink().replace("https://twitter.com/", "").replaceAll("\\?.*", "").replaceAll("\\/.*",
                    "");
            return twitter.searchUsers(name, 0).get(0).getId();
        } catch (TwitterException e) {
            log.error("Error while querying user id", e);
            return 0l;
        }
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new AddTwitterSubscriptionMessageHandler());
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

}