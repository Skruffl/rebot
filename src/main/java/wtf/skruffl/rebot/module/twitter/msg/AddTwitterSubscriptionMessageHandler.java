package wtf.skruffl.rebot.module.twitter.msg;

import java.util.LinkedHashMap;
import java.util.List;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.db.SubscriptionType;
import wtf.skruffl.rebot.core.db.dao.Subscription;
import wtf.skruffl.rebot.core.db.repo.SubscriptionRepository;
import wtf.skruffl.rebot.core.message.PrivilegedSubcommandNameMatchingHandler;
import wtf.skruffl.rebot.module.twitter.TwitterModule;

public class AddTwitterSubscriptionMessageHandler extends PrivilegedSubcommandNameMatchingHandler {

    public AddTwitterSubscriptionMessageHandler() {
        super("add", "twitter");
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        if (getArgs().length < 1) {
            return false;
        }

        Bot bot = getBot();
        String channelUrl = getArgs()[0];
        boolean shouldPing = getArgs().length > 1 ? "true".equalsIgnoreCase(getArgs()[1]) : false;
        if (!channelUrl.contains("twitter.com/")) {
            return false;
        }

        // remove all parameters attached to the url
        if (channelUrl.contains("?")) {
            channelUrl = channelUrl.substring(0, channelUrl.lastIndexOf("?"));
        }

        Subscription sub = null;
        SubscriptionRepository subRepo = bot.getRepos().getSubscriptionRepo();
        List<Subscription> subscriptions = subRepo.findAllByTypeAndGuildId(SubscriptionType.Twitter,
                event.getGuild().getIdLong());
        // check if a sub for the channel exists and remove if there is
        for (Subscription subscription : subscriptions) {
            if (subscription.getLink().equalsIgnoreCase(channelUrl)) {
                event.getChannel().sendMessage("Removed channel " + channelUrl).queue();
                subRepo.delete(subscription);
                return true;
            }
        }

        // else create a new one
        if (sub == null) {
            sub = new Subscription();
        }

        sub.setGuildId(event.getGuild().getIdLong());
        sub.setLink(channelUrl);
        sub.setType(SubscriptionType.Twitter);
        sub.setShouldPing(shouldPing);

        subRepo.save(sub);
        bot.getContext().getBean(TwitterModule.class).subscribeToAll();
        event.getChannel().sendMessage("Added channel " + channelUrl).queue();
        return true;
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        LinkedHashMap<String, String> args = new LinkedHashMap<>();
        args.put("channelUrl", "The link to the twitter account (has to be a twitter.com link");
        args.put("shouldPing",
                "Optional: if the notification role should be pinged for this twitter account (can be true or false)");
        event.getChannel().sendMessage(buildSyntax(getBot().getConfigDao().getPrefix(), "add twitter", args)).queue();
    }

}