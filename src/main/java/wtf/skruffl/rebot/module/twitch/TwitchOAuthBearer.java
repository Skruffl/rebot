package wtf.skruffl.rebot.module.twitch;

import java.io.IOException;

import com.google.gson.Gson;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import wtf.skruffl.rebot.core.WebUtils;
import wtf.skruffl.rebot.core.config.ConfigDao;

@Slf4j
@Component
public class TwitchOAuthBearer {

    private String token;
    private long expriation;

    private long creation;
    private String twitchToken;
    private String twitchSecret;

    private TwitchOAuthBearer(@Autowired ConfigDao configDao) {
        twitchToken = configDao.getTwitchApiToken();
        twitchSecret = configDao.getTwitchApiSecret();
        request();
    }

    private void request() {
        creation = System.currentTimeMillis();
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost post = new HttpPost(String.format(
                    "https://id.twitch.tv/oauth2/token?client_id=%s&client_secret=%s&grant_type=client_credentials",
                    twitchToken, twitchSecret));
            CloseableHttpResponse response = client.execute(post);
            AccessTokenReply atr = new Gson().fromJson(
                    WebUtils.getInstance().readFromInputStream(response.getEntity().getContent()),
                    AccessTokenReply.class);
            this.token = atr.getAccess_token();
            this.expriation = atr.getExpires_in() * 1000; // twitch returns in seconds but we're calculating in
                                                          // milliseconds
            log.debug("Received token {} and expiration {}", token, expriation);
        } catch (IOException e) {
            log.error("could not register oauth bearer", e);
        }
    }

    private boolean isValid() {
        long expiration = (this.creation + this.expriation) - 10000L; // add 10 second grace
        return expiration > System.currentTimeMillis();
    }

    public String getToken() {
        if (!isValid()) {
            request();
        }
        return token;
    }

    @Getter
    class AccessTokenReply {
        private String access_token;
        private String refresh_token;
        private long expires_in;
        private String scope;
        private String token_type;
    }
}