package wtf.skruffl.rebot.module.twitch.pojo;

import lombok.Data;

@Data
public class StreamInfo {
    private String id;
    private String user_id;
    private String user_name;
    private String game_id;
    private String[] community_ids;
    private String type;
    private String title;
    private int viewer_count;
    private String started_at;
    private String language;
    private String thumbnail_url;
}