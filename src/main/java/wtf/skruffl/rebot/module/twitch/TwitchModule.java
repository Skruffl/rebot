package wtf.skruffl.rebot.module.twitch;

import java.awt.Color;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.google.gson.JsonObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.WebUtils;
import wtf.skruffl.rebot.core.config.ConfigDao;
import wtf.skruffl.rebot.core.db.SettingType;
import wtf.skruffl.rebot.core.db.SubscriptionType;
import wtf.skruffl.rebot.core.db.dao.Setting;
import wtf.skruffl.rebot.core.db.dao.Subscription;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.twitch.msg.AddTwitchSubscriptionMessageHandler;
import wtf.skruffl.rebot.module.twitch.pojo.StreamInfo;
import wtf.skruffl.rebot.module.twitch.pojo.TwitchLiveResponse;
import wtf.skruffl.rebot.module.twitch.pojo.TwitchProfile;

@RestController
@Slf4j
@EnableScheduling
@RequestMapping("/api/v1/twitch")
public class TwitchModule extends BotModule {

    private Bot bot;

    private static final String NAME = "TWITCH";

    private boolean noConfiguredTokens = false;

    /**
     * maps the user name of the streamer mapped to the date they went live
     */
    private Map<String, String> livestreamMap = new HashMap<>();

    public TwitchModule(CommandRegistry registry, @Autowired Bot bot, @Autowired ConfigDao config) {
        super(NAME, registry);
        log.debug("Created new Twitch Module");
        this.bot = bot;

        if (config.getTwitchApiToken() == null || config.getTwitchApiSecret() == null) {
            noConfiguredTokens = true;
        }
    }

    @Scheduled(fixedRate = 1800000l, initialDelay = 1000l) // 30 minutes
    private void subscribeAll() {
        log.debug("Subscribing to all twitch channels");
        if (noConfiguredTokens) {
            return;
        }
        for (Subscription sub : bot.getRepos().getSubscriptionRepo().findAllByType(SubscriptionType.Twitch)) {
            try {
                log.debug("Subscribing to twitch streamer {}", sub.getLink());
                subscribe(sub);
            } catch (Exception e) {
                log.error("Couldn't subscribe to streamer {}", sub.getLink(), e);
            }
        }
    }

    public void subscribe(Subscription sub) {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            // content
            URIBuilder builder = new URIBuilder("https://api.twitch.tv/helix/webhooks/hub");
            JsonObject object = new JsonObject();
            object.addProperty("hub.mode", "subscribe");
            object.addProperty("hub.callback", WebUtils.getInstance().getCallbackUrl(bot.getPort()) + "api/v1/twitch/");
            object.addProperty("hub.topic", toFeed(sub.getLink()));
            object.addProperty("hub.lease_seconds", Long.toString(TimeUnit.MINUTES.toSeconds(30)));

            // create post
            HttpPost post = new HttpPost(builder.build());
            HttpEntity entity = new StringEntity(object.toString(), ContentType.APPLICATION_JSON);

            // add headers
            post.setEntity(entity);
            for (Map.Entry<String, String> entry : TwitchProfile.getHeaders(bot, false).entrySet()) {
                post.addHeader(entry.getKey(), entry.getValue());
            }

            // execute
            HttpResponse reply = client.execute(post);
            log.debug("Subscribing to twitch streamer {}", sub.getName());
            String content = WebUtils.getInstance().readFromInputStream(reply.getEntity().getContent());
            log.debug("Received content from twitch\n{}", content);
        } catch (IOException | URISyntaxException e) {
            log.error("Couldn't send posts", e);
        }
    }

    private String toFeed(String link) {
        StreamInfo info = new StreamInfo();
        info.setUser_name(link.substring(link.lastIndexOf('/') + 1));
        TwitchProfile profile = new TwitchProfile(bot, info);
        return "https://api.twitch.tv/helix/streams?user_id=" + profile.getUserId();
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new AddTwitchSubscriptionMessageHandler());
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

    @PostMapping
    public String receiveSomething(@RequestBody TwitchLiveResponse pushNotif) {
        log.debug("Received twitch endpoint json: \n{}", pushNotif);
        newStream(pushNotif);
        return "{\"message\":\"Success!\"}";
    }

    private boolean applyFilter(Subscription sub, StreamInfo info) {
        String link = sub.getLink();
        if (link.endsWith("/")) {
            link = link.substring(0, link.length() - 1);
        }
        int idx = link.lastIndexOf('/');
        String name = sub.getLink().substring(idx).replace("/", "");
        return name.equalsIgnoreCase(info.getUser_name());
    }

    private void newStream(TwitchLiveResponse pushNotif) {
        if (pushNotif.getData().length <= 0) {
            // ignore stream offline events
            return;
        }
        StreamInfo info = pushNotif.getData()[0];
        log.info("Sending new stream message for streamer {}", info.getUser_name());

        List<Subscription> subs = bot.getRepos().getSubscriptionRepo().findAllByType(SubscriptionType.Twitch);
        subs = subs.stream().filter(s -> applyFilter(s, info)).collect(Collectors.toList());

        String liveDate = livestreamMap.getOrDefault(info.getUser_name(), null);
        if (info.getStarted_at().equalsIgnoreCase(liveDate)) {
            log.info("Streamer {} is already live, not sending another notification", info.getUser_name());
            return;
        }

        livestreamMap.put(info.getUser_name(), info.getStarted_at());

        for (Subscription sub : subs) {
            sendMessage(sub, info);
        }
    }

    private void sendMessage(Subscription sub, StreamInfo info) {
        Guild guild = bot.getJda().getGuildById(sub.getGuildId());
        TextChannel channel = null;
        Role role = null;
        TwitchProfile profile = new TwitchProfile(bot, info);

        Setting channelSetting = bot.getRepos().fetchSetting(SettingType.TWITCH_NOTIFICATION_CHANNEL, sub.getGuildId());
        if (channelSetting == null) {
            channel = guild.getDefaultChannel();
        } else {
            channel = channelSetting.asChannel(guild);
        }

        Setting roleSetting = bot.getRepos().fetchSetting(SettingType.NOTIFICATION_ROLE_ID, sub.getGuildId());
        if (roleSetting != null) {
            role = roleSetting.asRole(guild);
        }

        String link = "https://twitch.tv/" + info.getUser_name();
        String bigPicture = profile.getBannerUrl();
        if (bigPicture == null) {
            bigPicture = info.getThumbnail_url();
        }
        bigPicture = bigPicture.replace("{width}", "1280").replace("{height}", "720");

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle(link);
        embedBuilder.setColor(new Color(255, 87, 175));
        embedBuilder.setThumbnail(profile.getAvatarUrl());
        embedBuilder.setImage(bigPicture);
        embedBuilder.setAuthor(info.getUser_name(), link, profile.getAvatarUrl());
        embedBuilder.addField("Playing", profile.getGameName(), false);
        embedBuilder.addField("Stream Title", info.getTitle() == null ? "---" : info.getTitle(), false);
        embedBuilder.addField("Followers", Integer.toString(profile.getFollowers()), true);
        embedBuilder.addField("Total Views", Integer.toString(profile.getTotalViews()), true);

        String message = "**" + info.getUser_name() + "** just went live!\nGo check it out!\n" + link;
        if (role != null && sub.isShouldPing()) {
            message = "Hey " + role.getAsMention() + "\n\n" + message;
        }

        Message msg = new MessageBuilder(embedBuilder.build()).setContent(message).build();
        channel.sendMessage(msg).queue();
    }

    @GetMapping
    public String challenge(
            @RequestParam(name = "hub.challenge", defaultValue = "No challenge present") String challenge) {
        log.debug("Received challenge: {}", challenge);
        return challenge;
    }

}