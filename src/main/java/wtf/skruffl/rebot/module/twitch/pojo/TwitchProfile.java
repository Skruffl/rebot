package wtf.skruffl.rebot.module.twitch.pojo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.WebUtils;
import wtf.skruffl.rebot.module.twitch.TwitchOAuthBearer;

@Data
@Slf4j
public class TwitchProfile {
    private String name;
    private String userId;
    private String avatarUrl;
    private String bannerUrl;
    private String gameName;
    private int followers;
    private int totalViews;
    private Bot bot;

    public TwitchProfile(Bot bot, StreamInfo info) {
        this.bot = bot;
        this.name = info.getUser_name();
        load(info);
    }

    private void load(StreamInfo info) {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            UserInfo userInfo = getUserInfo(client, info.getUser_name());
            if (userInfo != null) {
                name = info.getUser_name();
                userId = userInfo.getId();
                avatarUrl = userInfo.getProfile_image_url();
                totalViews = userInfo.getView_count();
            }

            GameInfo gameInfo = getGameInfo(client, info.getGame_id());
            if (gameInfo != null) {
                gameName = gameInfo.getName();
            }

            BannerInfo bannerInfo = getBannerInfo(client, userInfo.getId());
            if (bannerInfo != null) {
                bannerUrl = bannerInfo.getProfile_banner();
                followers = bannerInfo.getFollowers();
            }
        } catch (IOException e) {
            log.error("Couldn't fetch info about twitch user", e);
        }
    }

    public static Map<String, String> getHeaders(Bot bot, boolean isKraken) {
        Map<String, String> reqProps = new HashMap<>();
        reqProps.put("Client-ID", bot.getConfigDao().getTwitchApiToken());
        String token = bot.getContext().getBean(TwitchOAuthBearer.class).getToken();
        if (isKraken) {
            reqProps.put("Accept", "application/vnd.twitchtv.v5+json");
            reqProps.put("Authorization", "OAuth " + token);
        } else {
            reqProps.put("Authorization", "Bearer " + token);
        }
        return reqProps;
    }

    public UserInfo getUserInfo(HttpClient client, String name) {
        log.debug("Fetching user info for {}", name);
        try {
            URIBuilder builder = new URIBuilder("https://api.twitch.tv/helix/users");
            builder.addParameter("login", name);
            HttpGet get = new HttpGet(builder.build());
            for (Map.Entry<String, String> entry : getHeaders(bot, false).entrySet()) {
                get.addHeader(entry.getKey(), entry.getValue());
            }
            String content = WebUtils.getInstance().readFromInputStream(client.execute(get).getEntity().getContent());
            return new Gson().fromJson(content, UserReply.class).getData()[0];
        } catch (URISyntaxException | IOException e) {
            log.error("Couldn't create uri for twitch api call", e);
        }
        return null;
    }

    public GameInfo getGameInfo(HttpClient client, String gameId) {
        log.debug("Fetching game info for {}", gameId);
        try {
            URIBuilder builder = new URIBuilder("https://api.twitch.tv/helix/games");
            builder.addParameter("id", gameId);
            HttpGet get = new HttpGet(builder.build());
            for (Map.Entry<String, String> entry : getHeaders(bot, false).entrySet()) {
                get.addHeader(entry.getKey(), entry.getValue());
            }
            String content = WebUtils.getInstance().readFromInputStream(client.execute(get).getEntity().getContent());
            GameInfo[] data = new Gson().fromJson(content, GameReply.class).getData();
            if (data.length > 0) {
                return data[0];
            }
        } catch (URISyntaxException | IOException e) {
            log.error("Couldn't create uri for twitch api call", e);
        }
        return null;
    }

    public BannerInfo getBannerInfo(HttpClient client, String id) {
        log.debug("Fetching banner for {}", id);
        try {
            URIBuilder builder = new URIBuilder("https://api.twitch.tv/kraken/channels/" + id);
            HttpGet get = new HttpGet(builder.build());
            for (Map.Entry<String, String> entry : getHeaders(bot, true).entrySet()) {
                get.addHeader(entry.getKey(), entry.getValue());
            }
            String content = WebUtils.getInstance().readFromInputStream(client.execute(get).getEntity().getContent());
            return new Gson().fromJson(content, BannerInfo.class);
        } catch (URISyntaxException | IOException e) {
            log.error("Couldn't create uri for twitch api call", e);
        }
        return null;
    }

    @Data
    private class BannerInfo {
        private String _id;
        private String broadcaster_language;
        private String created_at;
        private String display_name;
        private int followers;
        private String game;
        private String language;
        private String logo;
        private boolean mature;
        private String name;
        private boolean partner;
        private String profile_banner;
        private String profile_banner_background_color;
        private String status;
        private String updated_at;
        private String url;
        private String video_banner;
        private int views;
    }

    @Data
    private class GameReply {
        private GameInfo[] data;
        private Pagination pagination;
    }

    @Data
    public class GameInfo {
        private String id;
        private String name;
        private String box_art_url;
    }

    @Data
    public class Pagination {
        private String cursor;
    }

    @Data
    public class UserReply {
        private UserInfo[] data;
    }

    @Data
    public class UserInfo {
        private String id;
        private String login;
        private String display_name;
        private String type;
        private String broadcaster_type;
        private String description;
        private String profile_image_url;
        private String offline_image_url;
        private int view_count;
        private String email;
    }
}