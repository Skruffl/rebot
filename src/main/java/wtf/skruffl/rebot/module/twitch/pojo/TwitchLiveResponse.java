package wtf.skruffl.rebot.module.twitch.pojo;

import lombok.Data;

@Data
public class TwitchLiveResponse {
    private StreamInfo[] data;
}