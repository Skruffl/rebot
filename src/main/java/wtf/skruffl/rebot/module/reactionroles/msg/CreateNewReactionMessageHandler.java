package wtf.skruffl.rebot.module.reactionroles.msg;

import java.util.LinkedHashMap;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.db.dao.ReactionRole;
import wtf.skruffl.rebot.core.message.NameMatchingMessageHandler;

@Slf4j
public class CreateNewReactionMessageHandler extends NameMatchingMessageHandler {

    private static final String NAME = "react";

    public CreateNewReactionMessageHandler() {
        super(NAME);
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        String msgId = "";
        String reaction = "";
        String roleId = "";
        boolean removable = true;
        try {
            msgId = getArgs()[0];
            reaction = getArgs()[1];
            roleId = getArgs()[2].replace("<@&", "").replace(">", "");
            removable = true;
            if (getArgs().length >= 4 && !Boolean.parseBoolean(getArgs()[3])) {
                removable = false;
            }
        } catch (Exception e) {
            log.debug("Couldn't parse message: {}", e.getMessage());
            return false;
        }

        ReactionRole reactionRole = new ReactionRole();
        reactionRole.setChannelId(event.getChannel().getIdLong());
        reactionRole.setGuildId(event.getGuild().getIdLong());
        reactionRole.setMessageId(Long.parseLong(msgId));
        reactionRole.setReaction(reaction);
        reactionRole.setRemoveOnReactionRemoval(removable);
        reactionRole.setRoleId(Long.parseLong(roleId));
        getBot().getRepos().getReactionRoleRepo().save(reactionRole);

        // regex that matches emote id like this:
        // <:nonbinary_pride_flag:676167766692069376>
        if (reaction.matches("\\<\\:.*\\:\\d*\\>")) {
            String emoteId = reaction.replaceFirst("\\<\\:.*\\:", "").replace(">", "");
            Emote emote = event.getGuild().getEmoteById(emoteId);
            event.getChannel().addReactionById(msgId, emote).queue();
        } else {
            event.getChannel().addReactionById(msgId, reaction).queue();
        }
        return true;
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("messageId", "The ID of the message to listen for reactions on");
        map.put("reaction", "The reaction emoji to use");
        map.put("roleId", "The ID of the role to hand out when the reaction is clicked");
        map.put("removable", "Optional, true or false. Users won't be able to remove the role if true");
        event.getChannel().sendMessage(buildSyntax(getBot().getConfigDao().getPrefix(), NAME, map)).queue();
    }

}