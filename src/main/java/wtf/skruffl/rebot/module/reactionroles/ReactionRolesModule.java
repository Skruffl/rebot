package wtf.skruffl.rebot.module.reactionroles;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.entities.MessageReaction.ReactionEmote;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.db.dao.ReactionRole;
import wtf.skruffl.rebot.core.db.repo.ReactionRoleRepository;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.reactionroles.msg.CreateNewReactionMessageHandler;

@Slf4j
public class ReactionRolesModule extends BotModule {

    private static final String NAME = "ReactionRoles";
    private Bot bot;

    public ReactionRolesModule(CommandRegistry registry, Bot bot) {
        super(NAME, registry);
        this.bot = bot;
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new CreateNewReactionMessageHandler());
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

    public void onGuildMessageReactionAdd(@Nonnull GuildMessageReactionAddEvent event) {
        log.debug("Added new reaction on message {}: {}", event.getMessageId(),
                event.getReaction().getReactionEmote().getAsReactionCode());
        ReactionRoleRepository reactionRepo = bot.getRepos().getReactionRoleRepo();
        List<ReactionRole> reactionRoles = reactionRepo.findAllByMessageId(event.getMessageIdLong());
        for (ReactionRole reactionRole : reactionRoles) {

            // check if the role still exists
            Role role = event.getGuild().getRoleById(reactionRole.getRoleId());
            if (role == null) {
                // role was deleted, delete obsolte reaction role
                reactionRepo.delete(reactionRole);
                continue;
            }

            // check if the emoji matches
            ReactionEmote reactionEmote = event.getReactionEmote();
            String emoji = reactionEmote.isEmoji() ? reactionEmote.getEmoji() : reactionEmote.getEmote().getAsMention();
            if (!emoji.equals(reactionRole.getReaction())) {
                continue;
            }
            // add role
            event.getGuild().addRoleToMember(event.getMember(), role).queue();
        }
    }

    public void onGuildMessageReactionRemove(@Nonnull GuildMessageReactionRemoveEvent event) {
        log.debug("Removed new reaction on message {}: {}", event.getMessageId(),
                event.getReaction().getReactionEmote().getAsReactionCode());
        ReactionRoleRepository reactionRepo = bot.getRepos().getReactionRoleRepo();

        List<ReactionRole> reactionRoles = reactionRepo.findAllByMessageId(event.getMessageIdLong());
        for (ReactionRole reactionRole : reactionRoles) {
            if (!reactionRole.isRemoveOnReactionRemoval()) {
                continue;
            }

            // check if the emoji matches
            ReactionEmote reactionEmote = event.getReactionEmote();
            String emoji = reactionEmote.isEmoji() ? reactionEmote.getEmoji() : reactionEmote.getEmote().getAsMention();
            log.debug("Reaction emoij: {}   Database Emoji: {}", emoji, reactionRole.getReaction());
            if (!emoji.equals(reactionRole.getReaction())) {
                continue;
            }

            Role role = event.getGuild().getRoleById(reactionRole.getRoleId());
            if (role == null) {
                // role was deleted, delete obsolte reaction role
                reactionRepo.delete(reactionRole);
                continue;
            }
            event.getGuild().removeRoleFromMember(event.getMember(), role).queue();
        }
    }

}