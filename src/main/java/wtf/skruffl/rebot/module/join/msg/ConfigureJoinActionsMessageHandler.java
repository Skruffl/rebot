package wtf.skruffl.rebot.module.join.msg;

import java.awt.Color;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.db.dao.JoinConfiguration;
import wtf.skruffl.rebot.core.db.repo.JoinConfigurationRepository;
import wtf.skruffl.rebot.core.message.NameMatchingMessageHandler;

@Slf4j
public class ConfigureJoinActionsMessageHandler extends NameMatchingMessageHandler {

    private static final String NAME = "join";

    public ConfigureJoinActionsMessageHandler() {
        super(NAME);
    }

    @Override
    public boolean handle(MessageReceivedEvent event) {
        String[] args = getArgs();
        JoinConfiguration config = null;

        JoinConfigurationRepository joinRepo = getBot().getRepos().getJoinRepo();
        try {
            List<JoinConfiguration> list = joinRepo.findAllByGuildId(event.getGuild().getIdLong());
            if (list.isEmpty()) {
                config = new JoinConfiguration();
            } else {
                config = list.get(0);
            }

            config.setGuildId(event.getGuild().getIdLong());

            if (args[0].equals("null") || args[0].trim().isEmpty()) {
                config.setRoleId(null);
            } else {
                // accept role pings and remove ping junk
                config.setRoleId(Long.parseLong(args[0].replace("<@&", "").replace(">", "")));
            }

            if (args[1].equals("null") || args[1].trim().isEmpty()) {
                config.setJoinMessageChannelId(null);
            } else {
                config.setJoinMessageChannelId(Long.parseLong(args[1].replace("<#", "").replace(">", "")));
            }

            if (args[2].equals("null") || args[2].trim().isEmpty()) {
                config.setEmbedColour(null);
            } else {
                // parse hex string to rgb value
                config.setEmbedColour(new Color(Integer.valueOf(args[2].substring(1, 3), 16),
                        Integer.valueOf(args[2].substring(3, 5), 16), Integer.valueOf(args[2].substring(5, 7), 16)));
            }

            StringBuilder builder = new StringBuilder();
            for (int i = 3; i > args.length; i++) {
                builder.append(args[i]);
            }
            if (!builder.toString().trim().isEmpty()) {
                config.setJoinMessageText(builder.toString());
            } else {
                config.setJoinMessageText(null);
            }
        } catch (Exception e) {
            log.debug("Exception while parsing message: {}", e.getMessage());
            return false;
        }
        joinRepo.save(config);
        return true;
    }

    @Override
    public void sendSyntax(MessageReceivedEvent event) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("roleId", "The role ID to give out when a new member joins (may be \"null\")");
        map.put("joinMessageChannelId", "The ID of the channel to send the join mesasge in. (may be \"null\")");
        map.put("embedColor", "The HexCode (#ff6900 for example) for the embed (may be \"null\")");
        map.put("joinMessageText",
                "The text to send in the join message (replaces $user with the user name and $guild with the guild name) (may be \"null\" or empty)");
        event.getChannel().sendMessage(buildSyntax(getBot().getConfigDao().getPrefix(), NAME, map)).queue();
    }

}