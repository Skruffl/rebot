package wtf.skruffl.rebot.module.join;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.BotModule;
import wtf.skruffl.rebot.core.CommandRegistry;
import wtf.skruffl.rebot.core.db.dao.JoinConfiguration;
import wtf.skruffl.rebot.core.message.MessageHandler;
import wtf.skruffl.rebot.module.join.msg.ConfigureJoinActionsMessageHandler;

public class JoinModule extends BotModule {

    private static final String NAME = "Join";
    private Bot bot;

    public JoinModule(CommandRegistry registry, Bot bot) {
        super(NAME, registry);
        this.bot = bot;
    }

    @Override
    public void registerModules() {
        List<MessageHandler> messages = Arrays.asList(new ConfigureJoinActionsMessageHandler());
        for (MessageHandler message : messages) {
            getRegistry().registerCommand(NAME, message);
        }
    }

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        Guild guild = event.getGuild();
        List<JoinConfiguration> joinConfigurations = bot.getRepos().getJoinRepo().findAllByGuildId(guild.getIdLong());
        if (joinConfigurations.isEmpty()) {
            return;
        }
        JoinConfiguration config = joinConfigurations.get(0);

        // add role
        Role role = guild.getRoleById(config.getRoleId());
        if (role != null) {
            guild.addRoleToMember(event.getMember(), role).queue();
        }

        // build and send message
        Message msg = buildMessage(event, config);
        if (msg != null) {
            TextChannel channel = null;
            if (config.getJoinMessageChannelId() != null) {
                channel = guild.getTextChannelById(config.getJoinMessageChannelId());
            } else {
                channel = guild.getDefaultChannel();
            }
            channel.sendMessage(msg).queue();
        }
    }

    private Message buildMessage(GuildMemberJoinEvent event, JoinConfiguration config) {
        EmbedBuilder builder = new EmbedBuilder();
        String text = config.getJoinMessageText();
        if (text == null) {
            return null;
        }
        Color colour = config.getEmbedColour();
        if (colour == null) {
            // default to a light blue
            colour = new Color(173, 216, 230);
        }
        builder.setTitle(text);
        builder.setAuthor(event.getGuild().getName(), event.getGuild().getIconUrl());
        builder.setColor(colour);
        builder.setFooter(event.getMember().getEffectiveName(), event.getMember().getUser().getAvatarUrl());
        return new MessageBuilder(builder).build();
    }

}