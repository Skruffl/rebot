package wtf.skruffl.rebot;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import wtf.skruffl.rebot.core.config.ConfigDao;

@Configuration
@EnableConfigurationProperties(ConfigDao.class)
public class AppConfig {

    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
        String path = System.getenv("REBOT_CONFIG");
        Resource resource = null;
        if (path == null || path.isEmpty()) {
            resource = new ClassPathResource("application.properties");
        } else {
            resource = new FileSystemResource(path);
        }
        properties.setLocation(resource);
        properties.setIgnoreResourceNotFound(false);
        return properties;
    }
}