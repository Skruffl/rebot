package wtf.skruffl.rebot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import wtf.skruffl.rebot.core.Bot;

@SpringBootApplication
public class Main {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Main.class, args);
		Bot bot = context.getBean(Bot.class);
		bot.start();
	}

}
