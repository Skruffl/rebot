package wtf.skruffl.rebot.core.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;
import wtf.skruffl.rebot.core.db.dao.Setting;
import wtf.skruffl.rebot.core.db.repo.JoinConfigurationRepository;
import wtf.skruffl.rebot.core.db.repo.PrivateRoomRepository;
import wtf.skruffl.rebot.core.db.repo.ReactionRoleRepository;
import wtf.skruffl.rebot.core.db.repo.SettingRepository;
import wtf.skruffl.rebot.core.db.repo.SubscriptionRepository;

@Component
@Getter
public class RepoHolder {
    @Autowired
    private SubscriptionRepository subscriptionRepo;
    @Autowired
    private ReactionRoleRepository reactionRoleRepo;
    @Autowired
    private SettingRepository settingRepo;
    @Autowired
    private JoinConfigurationRepository joinRepo;
    @Autowired
    private PrivateRoomRepository privateRoomRepo;

    public Setting fetchSetting(SettingType type, long id) {
        List<Setting> settings = settingRepo.findAllByTypeAndGuildId(type, id);
        if (settings.isEmpty()) {
            return null;
        }
        if (settings.size() > 1) {
            throw new RuntimeException("Settings query returned more than one result while expecting only one!");
        }
        return settings.get(0);
    }

    public List<Setting> fetchSettings(SettingType type, long id) {
        return settingRepo.findAllByTypeAndGuildId(type, id);
    }
}