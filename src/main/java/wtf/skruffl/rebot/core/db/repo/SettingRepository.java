package wtf.skruffl.rebot.core.db.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import wtf.skruffl.rebot.core.db.SettingType;
import wtf.skruffl.rebot.core.db.dao.Setting;

public interface SettingRepository extends CrudRepository<Setting, Long> {
    public List<Setting> findAllByTypeAndGuildId(SettingType type, long guildId);
}