package wtf.skruffl.rebot.core.db.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import wtf.skruffl.rebot.core.db.SubscriptionType;

@Data
@Entity
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private SubscriptionType type;

    private String link;
    private String name;
    private long guildId;
    private boolean shouldPing;

}