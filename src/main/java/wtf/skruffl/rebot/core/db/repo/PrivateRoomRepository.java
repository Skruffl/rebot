package wtf.skruffl.rebot.core.db.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import wtf.skruffl.rebot.core.db.dao.PrivateRoom;

public interface PrivateRoomRepository extends CrudRepository<PrivateRoom, Long> {
    public Optional<PrivateRoom> findOneByGuildId(long guildId);
}