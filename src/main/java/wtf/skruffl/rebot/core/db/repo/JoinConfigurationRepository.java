package wtf.skruffl.rebot.core.db.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import wtf.skruffl.rebot.core.db.dao.JoinConfiguration;

public interface JoinConfigurationRepository extends CrudRepository<JoinConfiguration, Long> {
    public List<JoinConfiguration> findAllByGuildId(long guildId);
}