package wtf.skruffl.rebot.core.db.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import wtf.skruffl.rebot.core.db.SettingType;

@Data
@Entity
public class Setting {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private long guildId;
    private String value;
    private SettingType type;

    public TextChannel asChannel(Guild guild) {
        if (guild.getIdLong() == guildId) {
            return guild.getTextChannelById(value);
        } else {
            throw new IllegalArgumentException("Guild does not match this setting");
        }
    }

    public Role asRole(Guild guild) {
        if (guild.getIdLong() == guildId) {
            return guild.getRoleById(value);
        } else {
            throw new IllegalArgumentException("Guild does not match this setting");
        }
    }

}