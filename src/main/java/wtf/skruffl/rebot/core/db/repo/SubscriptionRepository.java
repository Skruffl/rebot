package wtf.skruffl.rebot.core.db.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import wtf.skruffl.rebot.core.db.SubscriptionType;
import wtf.skruffl.rebot.core.db.dao.Subscription;

public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {
    public List<Subscription> findAllByType(SubscriptionType type);

    public List<Subscription> findAllByTypeAndGuildId(SubscriptionType type, long guildId);

    public List<Subscription> findAllByGuildId(long guildId);
}