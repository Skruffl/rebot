package wtf.skruffl.rebot.core.db;

public enum SubscriptionType {
    YouTube, Twitch, Twitter;
}