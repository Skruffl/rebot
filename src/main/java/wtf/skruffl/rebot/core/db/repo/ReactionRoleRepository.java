package wtf.skruffl.rebot.core.db.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import wtf.skruffl.rebot.core.db.dao.ReactionRole;

public interface ReactionRoleRepository extends CrudRepository<ReactionRole, Long> {
    public List<ReactionRole> findAllByMessageId(long messageId);
}