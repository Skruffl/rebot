package wtf.skruffl.rebot.core.db.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class ReactionRole {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private long guildId;
    private long channelId;
    private long messageId;

    private String reaction;

    private long roleId;

    /**
     * if false the user won't be able to get rid of the role by unreacting
     */
    private boolean removeOnReactionRemoval;
}