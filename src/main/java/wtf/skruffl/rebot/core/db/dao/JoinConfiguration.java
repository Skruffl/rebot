package wtf.skruffl.rebot.core.db.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.awt.Color;

import lombok.Data;

@Data
@Entity
public class JoinConfiguration {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Long guildId;
    private Long roleId;
    private Long joinMessageChannelId;
    private String joinMessageText;
    private Color embedColour;
}