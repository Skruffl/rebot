package wtf.skruffl.rebot.core;

import lombok.Getter;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public abstract class BotModule extends ListenerAdapter {
    @Getter
    private String name;
    @Getter
    private CommandRegistry registry;

    public BotModule(String name, CommandRegistry registry) {
        this.name = name;
        this.registry = registry;
    }

    public abstract void registerModules();
}