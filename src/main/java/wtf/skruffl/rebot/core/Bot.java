package wtf.skruffl.rebot.core;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.security.auth.login.LoginException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import wtf.skruffl.rebot.core.config.ConfigDao;
import wtf.skruffl.rebot.core.db.RepoHolder;
import wtf.skruffl.rebot.core.util.CacheList;
import wtf.skruffl.rebot.module.inpanix.VacancyModule;
import wtf.skruffl.rebot.module.join.JoinModule;
import wtf.skruffl.rebot.module.privatechannels.PrivateChannelsModule;
import wtf.skruffl.rebot.module.reactionroles.ReactionRolesModule;
import wtf.skruffl.rebot.module.standard.StandardModule;
import wtf.skruffl.rebot.module.twitch.TwitchModule;
import wtf.skruffl.rebot.module.twitter.TwitterModule;
import wtf.skruffl.rebot.module.youtube.YoutubeModule;

@Component
@Slf4j
public class Bot extends ListenerAdapter {

    @Autowired
    @Getter
    private RepoHolder repos;

    @Autowired
    @Getter
    private ConfigDao configDao;

    @Autowired
    @Getter
    private ApplicationContext context;

    @Autowired
    private CommandRegistry registry;

    @Getter
    private Set<BotModule> loadedModules = new HashSet<>();
    @Getter
    private JDA jda;
    @Getter
    private List<Message> messageCache = new CacheList<>(1000);

    @Getter
    @Value("${server.port}")
    private int port;

    /**
     * start up the discord bot, logging into the discord api
     */
    public void start() {

        try {
            log.info("Starting up discord bot!");
            jda = JDABuilder.create(Arrays.asList(GatewayIntent.values())).setToken(configDao.getToken()).build();
            jda.awaitReady();
            log.info("Bot successfully initialized!");
            jda.addEventListener(this);
            registerModules();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error("Thread interrupt while starting discord bot", e);
        } catch (LoginException e) {
            log.error("Couldn't sign into discord", e);
        }
    }

    private void registerModules() {
        List<BotModule> modules = Arrays.asList(context.getBean(TwitterModule.class),
                context.getBean(TwitchModule.class), new StandardModule(registry), new VacancyModule(registry),
                new YoutubeModule(registry, this), new ReactionRolesModule(registry, this),
                new JoinModule(registry, this), new PrivateChannelsModule(registry, this));
        for (BotModule module : modules) {
            loadedModules.add(module);
            module.registerModules();
            jda.addEventListener(module);
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        messageCache.add(event.getMessage());
        registry.fireCommand(event);
    }

}