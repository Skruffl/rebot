package wtf.skruffl.rebot.core.message;

import java.util.Map;
import java.util.Map.Entry;

import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.Bot;

public abstract class MessageHandler {

    /**
     * checks if this message handler is capable of handling the given message
     * 
     * @param jda the bot instance for jda and db access, may be null
     * @param msg the message to check
     * @return true, if it's capable, false if not
     */
    public abstract boolean applies(Bot bot, Message msg);

    /**
     * handles the given event, will fail if {@link this#applies(Message)} returns
     * false!
     * 
     * @param event the event to handle
     * @return true if the operation succeeded, false if not
     */
    public abstract boolean handle(MessageReceivedEvent event);

    /**
     * sends the syntax for this command
     * 
     * @param event the event to use the channel from
     */
    public abstract void sendSyntax(MessageReceivedEvent event);

    /**
     * creates a standardized syntax message
     * 
     * @param prefix the command prefix
     * @param name   the command name
     * @param args   the arguments, mapping argument name to argument description
     *               (make sure to use a linked hash map)
     */
    protected Message buildSyntax(String prefix, String name, Map<String, String> args) {
        StringBuilder builder = new StringBuilder();
        builder.append("**Usage:**\n");
        builder.append(prefix).append(name);
        builder.append(" ");
        for (String param : args.keySet()) {
            builder.append("<").append(param).append(">").append(" ");
        }
        builder.append("\n");
        for (Entry<String, String> entry : args.entrySet()) {
            builder.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }

        return new MessageBuilder().setContent(builder.toString()).build();
    }
}