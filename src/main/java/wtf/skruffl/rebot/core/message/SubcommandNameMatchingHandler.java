package wtf.skruffl.rebot.core.message;

import java.util.Arrays;

import lombok.Getter;
import net.dv8tion.jda.api.entities.Message;
import wtf.skruffl.rebot.core.Bot;

public abstract class SubcommandNameMatchingHandler extends MessageHandler {
    @Getter
    private String[] args;
    @Getter
    private Bot bot;
    private String command;
    private String subcommand;

    public SubcommandNameMatchingHandler(String command, String subcommand) {
        this.command = command;
        this.subcommand = subcommand;
    }

    @Override
    public boolean applies(Bot bot, Message msg) {
        this.bot = bot;
        String prefix = bot.getConfigDao().getPrefix();
        String messageContent = msg.getContentRaw();
        if (!messageContent.startsWith(prefix)) {
            return false;
        }
        String[] words = messageContent.split(" ");
        String commandName = words[0].substring(prefix.length());
        String subcommandName = words[1];
        args = Arrays.copyOfRange(words, 2, words.length);
        return command.equalsIgnoreCase(commandName) && subcommand.equals(subcommandName);
    }
}