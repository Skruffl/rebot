package wtf.skruffl.rebot.core.message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import net.dv8tion.jda.api.entities.Message;
import wtf.skruffl.rebot.core.Bot;

public abstract class NameMatchingMessageHandler extends MessageHandler {
    private String name;
    @Getter
    private String[] args;
    @Getter
    private Bot bot;

    public NameMatchingMessageHandler(String name) {
        this.name = name;
    }

    @Override
    public boolean applies(Bot bot, Message msg) {
        this.bot = bot;
        String prefix = bot.getConfigDao().getPrefix();
        String messageContent = msg.getContentRaw();
        if (!messageContent.startsWith(prefix)) {
            return false;
        }
        String[] words = splitArguments(messageContent);
        String commandName = words[0].substring(prefix.length());
        args = Arrays.copyOfRange(words, 1, words.length);
        return name.equalsIgnoreCase(commandName);
    }

    private String[] splitArguments(String messageContent) {
        String[] words = messageContent.split(" ");
        List<String> list = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        boolean unterminated = false;
        for (String word : words) {
            if (word.contains("\"")) {
                builder.append(word.replace("\"", "")).append(" ");
                if (unterminated) {
                    list.add(builder.toString().trim());
                    builder = new StringBuilder();
                    unterminated = false;
                } else {
                    unterminated = true;
                }
                continue;
            }

            if (unterminated) {
                builder.append(word.replace("\"", "")).append(" ");
                continue;
            }

            list.add(word);
        }

        return list.toArray(new String[list.size()]);
    }
}
