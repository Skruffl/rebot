package wtf.skruffl.rebot.core.message;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import wtf.skruffl.rebot.core.Bot;
import wtf.skruffl.rebot.core.db.SettingType;
import wtf.skruffl.rebot.core.db.dao.Setting;

public abstract class PrivilegedMessageHandler extends NameMatchingMessageHandler {
    public PrivilegedMessageHandler(String name) {
        super(name);
    }

    @Override
    public boolean applies(Bot bot, Message msg) {
        Setting modRole = bot.getRepos().fetchSetting(SettingType.MOD_ROLE_ID, msg.getGuild().getIdLong());
        Role role = null;
        if (modRole != null) {
            role = modRole.asRole(msg.getGuild());
        }

        boolean isOwner = msg.getGuild().getOwnerId().equals(msg.getAuthor().getId());
        boolean isMod = role != null && msg.getMember().getRoles().contains(role);
        return super.applies(bot, msg) && (isOwner || isMod);
    }
}