package wtf.skruffl.rebot.core.util;

import java.util.ArrayList;

/**
 * simple implementation of an array list with a max size. will remove the
 * oldest element if the maximum size is exceeded
 * 
 * @param <E> type of the object we're storing
 */
public class CacheList<E> extends ArrayList<E> {
    private static final long serialVersionUID = -5048365762669879905L;
    private int maxSize;

    /**
     * create a new cache list
     * 
     * @param maxSize the maximum size of this list
     */
    public CacheList(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(E e) {
        if (size() >= maxSize) {
            remove(0);
        }
        return super.add(e);
    }

}