package wtf.skruffl.rebot.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WebUtils {

    @Getter
    private static final WebUtils instance = new WebUtils();

    private WebUtils() {
        log.debug("Created new WebUtils!");
    }

    public String readFromInputStream(InputStream content) throws IOException {
        try (InputStream stream = content) {
            StringBuilder builder = new StringBuilder();
            int i = 0;
            while ((i = content.read()) != -1) {
                builder.append((char) i);
            }
            return builder.toString();
        }
    }

    public String getCallbackUrl(int port) throws IOException {
        URL whatismyip = new URL("http://checkip.amazonaws.com");
        try (BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));) {
            String publicIp = in.readLine(); // you get the IP as a String
            return String.format("http://%s:%s/", publicIp, port);
        }
    }
}