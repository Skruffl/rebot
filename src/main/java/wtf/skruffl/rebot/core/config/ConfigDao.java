package wtf.skruffl.rebot.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@ConfigurationProperties("bot")
@Component
@Getter
@Setter
@Slf4j
public class ConfigDao {
    private String token;
    private String prefix;
    @Value("${yt.token}")

    private String ytApiToken;
    @Value("${twitch.token}")
    private String twitchApiToken;
    @Value("${twitch.secret}")

    private String twitchApiSecret;
    @Value("${twitter.oauthkey}")
    private String twitterKey;
    @Value("${twitter.oauthsec}")
    private String twitterSec;
    @Value("${twitter.token}")
    private String twitterToken;
    @Value("${twitter.secret}")
    private String twitterSecret;

    public ConfigDao() {
        log.debug("Created new config dao!");
    }

    @Override
    public String toString() {
        // avoid leaking the token to log
        return "BotConfig";
    }
}
