package wtf.skruffl.rebot.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import wtf.skruffl.rebot.core.message.MessageHandler;

@Component
@Slf4j
public class CommandRegistry {

    @Autowired
    private ApplicationContext context;

    private Map<BotModule, Set<MessageHandler>> registeredMessages;

    public CommandRegistry() {
        registeredMessages = new HashMap<>();
    }

    public void registerCommand(String module, MessageHandler handler) {
        BotModule botModule = getModuleFromName(module);
        if (botModule == null) {
            throw new IllegalArgumentException("Module " + module + " is not registered!");
        }

        if (registeredMessages.containsKey(botModule)) {
            registeredMessages.get(botModule).add(handler);
        } else {
            Set<MessageHandler> set = new HashSet<>();
            set.add(handler);
            registeredMessages.put(botModule, set);
        }
        log.debug("Registered new command {} in module {}", handler.getClass().getName(), module);
    }

    private BotModule getModuleFromName(String name) {
        for (BotModule module : context.getBean(Bot.class).getLoadedModules()) {
            if (module.getName().equalsIgnoreCase(name)) {
                return module;
            }
        }
        return null;
    }

    public boolean fireCommand(MessageReceivedEvent event) {
        Message message = event.getMessage();
        if (message.getAuthor().isBot()) {
            return false;
        }
        Bot bot = context.getBean(Bot.class);
        Set<BotModule> modules = bot.getLoadedModules();
        int matchingHandlers = 0;
        for (Entry<BotModule, Set<MessageHandler>> entry : registeredMessages.entrySet()) {
            for (MessageHandler handler : entry.getValue()) {
                if (handler.applies(bot, message) && modules.contains(entry.getKey())) {
                    executeHandler(handler, event);
                    matchingHandlers++;
                }
            }
        }

        if (matchingHandlers <= 0) {
            log.debug("Message {}, {} has no matching handler", event.getMessage().getContentRaw(),
                    event.getMessage().getId());
            return false;
        }

        return true;
    }

    private void executeHandler(MessageHandler handler, MessageReceivedEvent event) {
        log.info("Detected Command {}", handler.getClass().getName());
        if (!handler.handle(event)) {
            handler.sendSyntax(event);
        }
    }
}