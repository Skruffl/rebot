FROM adoptopenjdk:14-jre-hotspot

RUN ["mkdir", "-p", "/var/rebot"]

COPY target/*.jar /var/rebot/rebot.jar

ENTRYPOINT ["java", "-jar", "/var/rebot/rebot.jar"]